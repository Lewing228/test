import base64
import imaplib
import email
import sqlite3
from datetime import datetime
from email.header import decode_header
from email.utils import parseaddr, parsedate_tz
import time
import configparser

config = configparser.ConfigParser()
config.read('config.ini', encoding='utf-8')

# Получение параметров для почтового ящика
mailbox_username = config.get('Mailbox', 'username')
mailbox_password = config.get('Mailbox', 'password')
mailbox_mailbox = config.get('Mailbox', 'mailbox')

# Получение параметров для анализатора
expected_senders = config.get('Analyzer', 'expected_senders').split(',')
valid_format_keywords = config.get('Analyzer', 'valid_format_keywords').split(',')

# Получение параметров для базы данных
database_path = config.get('Database', 'database_path')

# Получение общих параметров приложения
polling_interval = config.getint('Application', 'polling_interval')


def connect_to_mailbox(username, password, mailbox):
    mail = imaplib.IMAP4_SSL("imap.gmail.com")
    mail.login(username, password)
    mail.select(mailbox)
    return mail


def extract_data_from_email(msg):
    subject, encoding = decode_header(msg["Subject"])[0]
    if isinstance(subject, bytes):
        subject = subject.decode(encoding or "utf-8", errors="replace")

    body = b""
    for part in msg.walk():
        if part.get_content_type() == "text/plain":
            body = part.get_payload(decode=False)
            break
    return subject, body

def is_valid_format(subject, body, valid_format_keywords):
    return any(keyword in subject or keyword in body for keyword in valid_format_keywords)



def decode_base64(encoded_text):
    encodings = ['utf-8', 'latin-1']

    for encoding in encodings:
        try:
            decoded_text = base64.b64decode(encoded_text).decode(encoding, errors='replace')
            return decoded_text
        except UnicodeDecodeError:
            continue
    return None


def process_email_data(username, password, mailbox, database_path, expected_senders, valid_format_keywords):
    mail = connect_to_mailbox(username, password, mailbox)
    result, data = mail.search(None, "UNSEEN")  # Получаем только непрочитанные сообщения
    if result == "OK":
        for num in data[0].split():
            _, msg_data = mail.fetch(num, "(RFC822)")
            msg = email.message_from_bytes(msg_data[0][1])
            _, sender_email = parseaddr(msg.get("From"))
            date = msg.get("Date")
            mail_date = datetime.strptime(date, "%a, %d %b %Y %H:%M:%S %z").strftime("%Y-%m-%d %H:%M:%S")
            if sender_email in expected_senders:
                subject, body = extract_data_from_email(msg)
                decoded_text = decode_base64(body)

                if is_valid_format(subject, decoded_text, valid_format_keywords):
                    connection = sqlite3.connect(database_path)
                    cursor = connection.cursor()

                    cursor.execute('''
                        CREATE TABLE IF NOT EXISTS emails (
                            id INTEGER PRIMARY KEY AUTOINCREMENT,
                            subject TEXT,
                            body TEXT,
                            sender TEXT,
                            Date TEXT
                        )
                    ''')

                    cursor.execute('''
                        INSERT INTO emails (subject, body, sender, Date) VALUES (?, ?, ?, ?)
                    ''', (subject, decoded_text, sender_email, mail_date))

                    connection.commit()
                    connection.close()

    mail.logout()


def update_config():
    global mailbox_username, mailbox_password, mailbox_mailbox, expected_senders, valid_format_keywords, database_path, polling_interval

    config.read('config.ini', encoding='utf-8')

    mailbox_username = config.get('Mailbox', 'username')
    mailbox_password = config.get('Mailbox', 'password')
    mailbox_mailbox = config.get('Mailbox', 'mailbox')

    expected_senders = config.get('Analyzer', 'expected_senders').split(',')
    valid_format_keywords = config.get('Analyzer', 'valid_format_keywords').split(',')

    database_path = config.get('Database', 'database_path')
    polling_interval = config.getint('Application', 'polling_interval')


while True:
    update_config()
    process_email_data(mailbox_username, mailbox_password, mailbox_mailbox, database_path, expected_senders, valid_format_keywords)
    time.sleep(polling_interval)
